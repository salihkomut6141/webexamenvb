interface Movie {
    "title" : String,
    "year" : number,
    "actors" : string[],
    "metascore" : number,
    "seen" : boolean
}
interface the90s {
    "title": string,
    "year": number
}
interface MetaScore {
    "title": string,
    "metascore": number
}
interface FakeAssMetaScore {
    "title": string,
    "metascore": number
}

let thematrix: Movie = {
    title: "The Matrix",
    year: 1999,
    actors: ["Keanu reeves", "Laurence Fishburne", "Carrie-Anne Moss"],
    metascore: 73,
    seen: true
}

let myfavoritemovie: Movie = {
    title: "Deadpool",
    year: 2016,
    actors: ["Ryan Reynolds", "Bla plada", "Bla Bla"],
    metascore: 95,
    seen: true
}

let myworstmovie: Movie = {
    title: "Assasin's Creed",
    year: 2016,
    actors: ["Michael Fassbender", "Bla Bla BLa"],
    metascore: 45,
    seen: true
}

const wasMovieMadeInThe90s = (movie: Movie) => {
    if (movie.year < 2000 && movie.year > 1989) {
        return true
    }
    else {
        return false
    }
}

const averageMetaScore = (movies: Array<Movie>) => {
    let sum: number = 0;
    let result: number = 0;

    for (let i:number = 0; i < movies.length; i++) 
    {
        sum += movies[i].metascore;
    }

    result = sum / movies.length;
    return result;
}

const fakeMetaScore = (movie: Movie, newscore: number) => {
    movie.metascore = 100;
    return movie;
}
console.log(fakeMetaScore(myworstmovie,100));