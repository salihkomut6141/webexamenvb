interface printInformatie {
    (number: number): void
}

const printToConsole = (number : number) : void => console.log(`the result is ${number}`); 

const sum = (number: Array<number>, info: printInformatie, info2: printInformatie): void => {
    let result: number = 0;
    for (let i: number = 0; i< number.length; i++) {
            result += number[i];
    }
    if (result > 10)
    {
        info(result);
    }
    else
    {
        info2(result);
    }
}

sum([1,2,5],printToConsole, number => console.log(`the result is ${number}. it is a very small number`));