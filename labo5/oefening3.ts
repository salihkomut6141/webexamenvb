interface Calculation {(a:number, b:number): number}

const add: Calculation = (a,b): number => a+b;
const mult: Calculation = (a,b): number => a*b;

const printCalculation = (a:number, b:number, func: Calculation): void => {
    console.log(func(a,b));
}

printCalculation(12,20,add);
printCalculation(12,20,mult);

export{}