interface PrintStuff {
    (amount: number, text: string):void
}

const printStuff: PrintStuff = (amount, text) => console.log(`hello ${text}, you are number ${amount}`);

interface TwoDArray {
    (element1: string, element2: string): string[]
}

const twoDarray: TwoDArray = (element1, element2) => {return [element1, element2];}

interface NumberToString {
    (number: number): void
}

const numberToString: NumberToString = (number) => {return `${number}`};
