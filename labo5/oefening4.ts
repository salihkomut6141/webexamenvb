const calculateAnimalAge = (age:number, ratio:number): number => age * ratio;

interface CalculateSpecificAnimalAge {
    (age:number): number
}

const calculateAnimalAgeFunctional = (ratio:number): CalculateSpecificAnimalAge => {
    return age => calculateAnimalAge(age,ratio);
}

const calculateDogAge = calculateAnimalAgeFunctional(7);
const calculateHamsterAge = calculateAnimalAgeFunctional(.5);

console.log(calculateDogAge(10));
console.log(calculateHamsterAge(10));