const express = require('express');
const fetch = require('node-fetch');
const app = express();
const ejs = require('ejs');

app.set('port', 5000);
app.set('view engine', 'ejs');

let thisisme = {
    name: "salih",
    age: 22,
    profilepic: "https://media.wired.com/photos/598e35994ab8482c0d6946e0/master/w_1024%2Cc_limit/phonepicutres-TA.jpg"
}

let pikachu: any = {};
const getPikachu = async() => {
    let response = await fetch('https://pokeapi.co/api/v2/pokemon/pikachu');
        pikachu = await response.json();
};
getPikachu();


app.get('/',(req:any,res:any)=>{
    res.render('index')
});

app.get('/whoami',(req:any,res:any)=>{
    res.render('whoami',{data:thisisme});
});

app.get('/whoamijson',(req:any,res:any)=>{
    res.json(thisisme);
});

app.get('/pikachujson',(req:any,res:any)=>{
    res.json(pikachu);
});

app.get('/pikachuhtml',(req:any,res:any)=>{
    res.render('pikachu',{pikachu: pikachu});
});

app.listen(app.get('port'),()=>console.log( ' [server] http://localhost:' + app.get('port')));

export{};