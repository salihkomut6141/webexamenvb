enum regenboog 
{
    rood,
    oranje,
    geel,
    groen,
    blauw,
    indigo,
    violet
}

let regenboogArray: Array<string> = [];

for (let i: number = 0; i < 7; i++)
{
    regenboogArray.push(regenboog[i]);
    console.log(regenboogArray[i]);
}

while(regenboogArray.length > 0)
{
    console.log(regenboogArray.pop());
}