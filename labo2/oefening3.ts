let veelvoudenZeven: Array<number> = [];

for (let i:number = 0; i < 70 + 1; i++)
{
    if (i % 7 === 0)
    {
        veelvoudenZeven.push(i);
    }
}

for (let i:number = 0; i < veelvoudenZeven.length; i++)
{
    if (veelvoudenZeven[i] % 5 === 0 && veelvoudenZeven[i] % 3 != 0)
    {
        // console.log(veelvoudenZeven[i]);
    }
    else if (veelvoudenZeven[i] % 3 === 0 && veelvoudenZeven[i] % 5 != 0)
    {
        // console.log(veelvoudenZeven[i]);
    }
}

//zelfde loop als hierboven maar met gebruik van ternary operator
for (let i:number = 0; i < veelvoudenZeven.length; i++)
{
    (veelvoudenZeven[i] % 5 === 0 && veelvoudenZeven[i] % 3 != 0 || veelvoudenZeven[i] % 3 === 0 && veelvoudenZeven[i] % 5 != 0? console.log(veelvoudenZeven[i]) : "geen veelvoud van 5 OF 3");
}