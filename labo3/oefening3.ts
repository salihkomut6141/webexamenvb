const multiplyText = (amount: number, text: string = "default", appendix?:string) => {
    let result: string = text;
    for(let i:number = 0; i < amount-1; i++){
        result += " " + text;
    }
    if(appendix){
        result += appendix;
    }
    return result;
}
console.log(multiplyText(3, undefined, "?"));