const chalk = require('chalk');
enum Color {
    Red,
    Green,
    Blue
}

const multiplyTextColor = (amount: number, text: string = "default",  color: Color, appendix?:string) => {
    let result: string = text;
    for(let i:number = 0; i < amount-1; i++){
        result += " " + text;
    }
    if(appendix){
        result += appendix;
    }
    switch(+color){
        case Color.Red:
            console.log(chalk.red(result));
            break;
        case Color.Green:
            console.log(chalk.green(result));
            break;
        case Color.Blue:
            console.log(chalk.blue(result));
            break;
        default:
            console.log(result, "De door uw gekozen kleur kan niet gebruikt worden.")
    }
}

multiplyTextColor(5, undefined,Color.Red, "!");
