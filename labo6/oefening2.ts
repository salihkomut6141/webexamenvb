let names: string[] = ['joske','franske','donald','achmed'];
let capitalNames1: string[] = [];
let capitalNames2: string[] = [];
//forEach methode 
names.forEach(e=>capitalNames1.push(e.toUpperCase()));
console.log(capitalNames1);

//map methode
capitalNames2 = names.map(e=>e.toUpperCase());
console.log(capitalNames2);