const fetch = require('node-fetch');

const doFetch = async()=>{
        let pagenumber = 1;
        while(true) {
            let response = await fetch('https://icanhazdadjoke.com/search?term=dog&limit=5&page=' + pagenumber, {
                headers: {
                    'Accept': 'application/json'
                }
            });
        let jsondata = await response.json();
        jsondata.results.forEach((e: any) => console.log(e.joke));   
        if (pagenumber >= jsondata.next_page){
            break;
        }
        console.log(pagenumber);
        pagenumber++;
    }
}


doFetch();

export{};